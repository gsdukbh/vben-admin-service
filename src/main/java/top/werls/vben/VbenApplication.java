package top.werls.vben;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author leejiawei
 */
@SpringBootApplication
public class VbenApplication {

    public static void main(String[] args) {
        SpringApplication.run(VbenApplication.class, args);
    }

}
